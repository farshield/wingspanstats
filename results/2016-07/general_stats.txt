General statistics
--------------------------------------------
Total kills: 880
Total value: 99.21b
Average value/kill: 112.73m
-- Total number of active pilots: 93/383 (24.28%)
Kills per active member: 9.46
ISK destroyed per active member: 1066.72m
Solo total kills: 483
Solo total value: 24.30b
Citadel kills: 4

High-sec total kills: 12 (1.36%)
High-sec total value: 1.15b (1.16%)

Low-sec total kills: 186 (21.14%)
Low-sec total value: 7.61b (7.67%)

Null-sec total kills: 95 (10.80%)
Null-sec total value: 13.79b (13.90%)

W-space total kills: 587 (66.70%)
W-space total value: 76.66b (77.27%)
  [*] C1 - total kills: 191, total isk: 9.11b
  [*] C2 - total kills: 218, total isk: 25.05b
  [*] C3 - total kills: 125, total isk: 22.52b
  [*] C4 - total kills: 38, total isk: 6.91b
  [*] C5 - total kills: 12, total isk: 12.93b
  [*] C13 - total kills: 3, total isk: 0.14b
