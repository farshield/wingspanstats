Solo ships destroyed
--------------------------------------------
#01 - Smug McSwaggin - 31 ships
#02 - Orianna Okanata - 20 ships
#03 - Chance Ravinne - 18 ships
#04 - Chessur - 17 ships
#05 - Saladiin - 7 ships
#06 - Herbert Uromastyx - 5 ships
#07 - Otho Bunce - 5 ships
#08 - Skye Shoal - 4 ships
#09 - Enki Parker - 4 ships
#10 - Glade Radner - 3 ships
#11 - Inoshin Drunkenpaw - 3 ships
#12 - Naughty Ninjette - 2 ships
#13 - uk badboy - 2 ships
#14 - Syrvana Nox - 1 ships
#15 - forsakenZen - 1 ships
#16 - Cantsee Tokila - 1 ships

Solo ISK destroyed
--------------------------------------------
#01 - Herbert Uromastyx - 0.85b
#02 - Smug McSwaggin - 0.83b
#03 - Orianna Okanata - 0.80b
#04 - Chance Ravinne - 0.40b
#05 - Otho Bunce - 0.30b
#06 - Chessur - 0.27b
#07 - Saladiin - 0.15b
#08 - Naughty Ninjette - 0.09b
#09 - Skye Shoal - 0.08b
#10 - Cantsee Tokila - 0.07b
#11 - Inoshin Drunkenpaw - 0.03b
#12 - Glade Radner - 0.02b
#13 - Syrvana Nox - 0.01b
#14 - uk badboy - 0.00b
#15 - Enki Parker - 0.00b
#16 - forsakenZen - 0.00b
