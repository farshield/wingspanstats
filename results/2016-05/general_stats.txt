General statistics
--------------------------------------------
Total kills: 1015
Total value: 129.54b
Average value/kill: 127.62m
-- Total number of active pilots: 188/424 (44.34%)
Kills per active member: 5.40
ISK destroyed per active member: 689.03m
Solo total kills: 600
Solo total value: 30.26b
Citadel kills: 10

High-sec total kills: 101 (9.95%)
High-sec total value: 1.22b (0.94%)

Low-sec total kills: 176 (17.34%)
Low-sec total value: 8.74b (6.75%)

Null-sec total kills: 109 (10.74%)
Null-sec total value: 12.00b (9.26%)

W-space total kills: 629 (61.97%)
W-space total value: 107.58b (83.05%)
  [*] C1 - total kills: 109, total isk: 13.81b
  [*] C2 - total kills: 234, total isk: 37.18b
  [*] C3 - total kills: 192, total isk: 25.13b
  [*] C4 - total kills: 43, total isk: 15.04b
  [*] C5 - total kills: 39, total isk: 14.90b
  [*] C6 - total kills: 8, total isk: 1.43b
  [*] Thera - total kills: 4, total isk: 0.09b
