Top BLOPS pilots - ships destroyed
--------------------------------------------
#01 - Saria Dehmov - 81 ships
#02 - Chance Ravinne - 35 ships
#03 - Ransom Odunen - 31 ships
#04 - togi yuki - 19 ships
#05 - Kanly Aideron - 19 ships
#06 - Gillion Wyrddych - 15 ships
#07 - Sabrez Haklar - 15 ships
#08 - More11o - 15 ships
#09 - Xeromos - 12 ships
#10 - Cynthera Noir - 12 ships
#11 - Abaddon Nergal - 11 ships
#12 - Junako Zelkarr - 10 ships
#13 - Nicholas Chance Kile - 9 ships
#14 - Vince Condor - 8 ships
#15 - lashana darkmoon - 8 ships
#16 - Inta Vakaria - 8 ships
#17 - Erika Tsurpalen - 6 ships
#18 - Vothor - 5 ships
#19 - Tali Lyrae - 5 ships
#20 - Glade Radner - 5 ships
#21 - Cyran Reinhard - 4 ships
#22 - Bails Naskingar - 4 ships
#23 - Rnubby18 - 3 ships
#24 - Bleda Katona - 3 ships
#25 - Newbus - 3 ships

Top BLOPS pilots - ISK destroyed
--------------------------------------------
#01 - Saria Dehmov - 24.76b
#02 - Ransom Odunen - 16.23b
#03 - Kanly Aideron - 9.03b
#04 - togi yuki - 7.83b
#05 - Gillion Wyrddych - 7.01b
#06 - More11o - 5.88b
#07 - Sabrez Haklar - 5.63b
#08 - Abaddon Nergal - 5.02b
#09 - lashana darkmoon - 4.78b
#10 - Junako Zelkarr - 3.53b
#11 - Bails Naskingar - 3.37b
#12 - Cynthera Noir - 3.33b
#13 - Inta Vakaria - 3.31b
#14 - Xeromos - 2.79b
#15 - Tali Lyrae - 2.52b
#16 - Chance Ravinne - 2.45b
#17 - Nicholas Chance Kile - 1.98b
#18 - Newbus - 1.95b
#19 - Glade Radner - 1.88b
#20 - Erika Tsurpalen - 1.76b
#21 - Vince Condor - 1.13b
#22 - Vothor - 0.77b
#23 - Rnubby18 - 0.68b
#24 - Malak Saren - 0.53b
#25 - Dermia - 0.45b
