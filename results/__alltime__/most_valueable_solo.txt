Most valueable solo kills
--------------------------------------------
#01 - https://zkillboard.com/kill/55776963/ - Findlaech - 17.41b
#02 - https://zkillboard.com/kill/50845467/ - Aravorn Nighthawk - 5.30b
#03 - https://zkillboard.com/kill/45955236/ - Cap'n Rick Clusterbomb - 5.21b
#04 - https://zkillboard.com/kill/50845404/ - Aravorn Nighthawk - 5.00b
#05 - https://zkillboard.com/kill/48675320/ - Valtyr Farshield - 4.86b
#06 - https://zkillboard.com/kill/49210279/ - Lainis Pootis - 4.14b
#07 - https://zkillboard.com/kill/48402752/ - NovaMoon Enaka - 3.47b
#08 - https://zkillboard.com/kill/52515824/ - Severin Talvan - 2.80b
#09 - https://zkillboard.com/kill/46845263/ - Sagwald Tian - 2.79b
#10 - https://zkillboard.com/kill/43869333/ - Enki Parker - 2.76b
#11 - https://zkillboard.com/kill/54159216/ - Massive Black - 2.62b
#12 - https://zkillboard.com/kill/52377766/ - Al3x Oaks - 2.56b
#13 - https://zkillboard.com/kill/43962303/ - Dexter Tripod - 2.26b
#14 - https://zkillboard.com/kill/50059165/ - Eridanii - 2.19b
#15 - https://zkillboard.com/kill/52515868/ - Severin Talvan - 2.19b
#16 - https://zkillboard.com/kill/56797049/ - Myra Vixen - 2.09b
#17 - https://zkillboard.com/kill/54206384/ - Shahta Kopatel - 2.08b
#18 - https://zkillboard.com/kill/55511051/ - Narcisse Nightmare - 2.02b
#19 - https://zkillboard.com/kill/55326048/ - Tasmine Cha-Tien - 2.00b
#20 - https://zkillboard.com/kill/43835798/ - shepard locknear - 1.99b
#21 - https://zkillboard.com/kill/48402727/ - NovaMoon Enaka - 1.89b
#22 - https://zkillboard.com/kill/52968445/ - Richter Trench - 1.54b
#23 - https://zkillboard.com/kill/56347916/ - Tasmine Cha-Tien - 1.52b
#24 - https://zkillboard.com/kill/48810183/ - WeirdBall - 1.51b
#25 - https://zkillboard.com/kill/57082021/ - SoftCore - 1.50b
